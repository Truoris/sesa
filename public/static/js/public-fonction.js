function getXMLHttpRequest() {
	var xhr = null;
	
	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest();
		}
	} else {
		alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest.");
		return null;
	}
	
	return xhr;
}

function disableCheck(inputId)
{
	if (document.getElementById(inputId+"_0").checked)
	{
		document.getElementById(inputId+"_autre").disabled = false;
	}
	else
	{
		document.getElementById(inputId+"_autre").disabled = true;
	}
}

function disableSelect(inputId)
{
	var select = document.getElementById(inputId);
	
	var max = select.options.length;
	var autre = false;
	for (i=0 ; i< max ; i++)
	{
		if (select.options[i].selected && select.options[i].value == "sesaAutre")
		{
			autre = true;
		}
	}
	
	if (autre)
	{
		document.getElementById(inputId+"_autre").disabled = false;
	}
	else
	{
		document.getElementById(inputId+"_autre").disabled = true;
	}
}

function mdp(id)
{
	rep = "";
	while (rep == null || rep == "")
	{
		var rep = prompt("Mot de Passe", "");
		
		if (rep != null && rep != "")
		{
			rep = encodeURIComponent(rep);
			var xhr = getXMLHttpRequest();
		
			if (xhr && xhr.readyState != 0) {
				xhr.abort();
				delete xhr;
			}
			
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4)
				{
					if (xhr.status == 200)
					{
						console.log(xhr.responseText);
						if (xhr.responseText == "none")
						{
							alert('Mot de passe incorrect');
							mdp(id);
						}
						else if (xhr.responseText == "ok")
						{
							document.getElementById("page").className = "container";
						}
					}
					else
					{
						mdp(id);
					}
				}
			}
			
			xhr.open("POST", "mdp-check.html", true);
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xhr.send("mdp="+rep+"&id="+id);
		}
	}
}
