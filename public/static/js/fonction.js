function getXMLHttpRequest() {
	var xhr = null;
	
	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest();
		}
	} else {
		alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest.");
		return null;
	}
	
	return xhr;
}

function design()
{
	height = window.innerHeight-190;
    
    document.getElementById("viewFrame").style.height = (height)+"px";
    height -= 45;
    document.getElementById("formCode").style.height = (height)+"px";
}

function save(type)
{
	var code = encodeURIComponent(document.getElementById('formCode').value);
	var nom = encodeURIComponent(document.getElementById('questionnaireNom').value);
	
	if (code != "" && nom != "")
	{
		document.getElementById('erreurNom').className = "control-group";
		var xhr = getXMLHttpRequest();
		
		if (xhr && xhr.readyState != 0) {
			xhr.abort();
			delete xhr;
		}
		
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200)
			{
				location.replace('admin/');
			}
		}
		
		if (type == "edit")
		{
			var id = encodeURIComponent(document.getElementById('formId').value);
			var slug = encodeURIComponent(document.getElementById('formSlug').value);
	
			xhr.open("POST", "admin/edit-"+id+"-"+slug+".html", true);
		}
		else
		{
			xhr.open("POST", "admin/save.html", true);
		}
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xhr.send("code="+code+"&nom="+nom);
	}
	else if (nom == "")
	{
		document.getElementById('erreurNom').className = "control-group error";
	}
}

function refreshView()
{
	var code = encodeURIComponent(document.getElementById('formCode').value);
	var nom = encodeURIComponent(document.getElementById('questionnaireNom').value);
	
	if (code != "" && nom != "")
	{
		document.getElementById('erreurNom').className = "control-group";
		var xhr = getXMLHttpRequest();
		
		if (xhr && xhr.readyState != 0) {
			xhr.abort();
			delete xhr;
		}
		
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200)
			{
				console.log('ok');
				rep = xhr.responseText.split('::::::');
				document.getElementById('viewFrame').src = "admin/view-"+rep[1]+".html";
			}
		}
		
		xhr.open("POST", "admin/parser.html", true);
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xhr.send("code="+code+"&nom="+nom);
	}
	else if (nom == "")
	{
		document.getElementById('erreurNom').className = "control-group error";
	}
}

function listeToString(choix)
{
	var tab= new RegExp('\r\n|\r|\n');
	var choix = choix.split(tab);
	
	var longueur = choix.length;
	var retour = choix[0];
	
	for (i=1 ; i<longueur ; i++)
	{
		if (choix[i] != "")
		{
			retour += "::"+choix[i];
		}
	}
	
	return retour;
}

function listeToStrLigne(choix)
{
	var tab= new RegExp('\r\n|\r|\n');
	var choix = choix.split(tab);
	
	var longueur = choix.length;
	var retour = "ID:"+choix[0];
	
	for (i=1 ; i<longueur ; i++)
	{
		if (choix[i] != "")
		{
			retour += "::ID:"+choix[i];
		}
	}
	return retour;
}

function insertCode(type, mode)
{
	var id = "\n<ID:!:";

	
	if (type == "checkbox")
	{
		var nom = document.getElementById('champCheckboxNom').value;
		var choix = document.getElementById('champCheckboxChoix').value;
		var autre =document.getElementById("champCheckboxAutre");
		
		if (nom != "")
		{
	 		strChoix = listeToString(choix);
	 		
	 		var txtarea = document.getElementById('formCode');
	 		var position = txtarea.selectionStart;
	 		var code = txtarea.value;
	 	
			debut = code.substr(0, position); 
			fin = code.substr(position); 
			
			checkbox_normal=document.getElementById("id_d_une_check_box_normal")

			if(autre.checked)
			{
				code = debut+id+"caseMultiple:!:"+nom+":!:"+strChoix+":!:autre:1:!:>\n"+fin;
			}
			else
			{
				code = debut+id+"caseMultiple:!:"+nom+":!:"+strChoix+":!:autre:0:!:>\n"+fin;
			}
			
			txtarea.value = code;
			
			$('#formCheckbox').modal('hide');
			
			document.getElementById('champCheckboxNom').value = "";
			document.getElementById('champCheckboxChoix').value = "";
		}
	}
	else if (type == "radio")
	{
		var nom = document.getElementById('champRadioNom').value;
		var choix = document.getElementById('champRadioChoix').value;
		var autre =document.getElementById("champRadioAutre");
		
		if (nom != "")
		{
			strChoix = listeToString(choix);
			
	 		var txtarea = document.getElementById('formCode');
	 		var position = txtarea.selectionStart;
	 		var code = txtarea.value;
	 	
			debut = code.substr(0, position); 
			fin = code.substr(position); 
			
			if(autre.checked)
			{
				code = debut+id+"caseUnique:!:"+nom+":!:"+strChoix+":!:autre:1:!:>\n"+fin;
			}
			else
			{
				code = debut+id+"caseUnique:!:"+nom+":!:"+strChoix+":!:autre:0:!:>\n"+fin;
			}
			
			txtarea.value = code;
			
			$('#formRadio').modal('hide');
			
			document.getElementById('champRadioNom').value = "";
			document.getElementById('champRadioChoix').value = "";
		}
	}
	else if (type == "select")
	{
		var nom = document.getElementById('champSelectNom').value;
		var choix = document.getElementById('champSelectChoix').value;
		var autre =document.getElementById("champSelectAutre");
		
		if (nom != "")
		{
			strChoix = listeToString(choix);
	 		
	 		var txtarea = document.getElementById('formCode');
	 		var position = txtarea.selectionStart;
	 		var code = txtarea.value;
	 	
			debut = code.substr(0, position); 
			fin = code.substr(position); 
			
			if(autre.checked)
			{
				code = debut+id+"selectUnique:!:"+nom+":!:"+strChoix+":!:autre:1:!:>\n"+fin;
			}
			else
			{
				code = debut+id+"selectUnique:!:"+nom+":!:"+strChoix+":!:autre:0:!:>\n"+fin;
			}

			txtarea.value = code;
			
			$('#formSelect').modal('hide');
			
			document.getElementById('champSelectNom').value = "";
			document.getElementById('champSelectChoix').value = "";
		}
	}
	else if (type == "selectmultiple")
	{
		var nom = document.getElementById('champSelectMultipleNom').value;
		var choix = document.getElementById('champSelectMultipleChoix').value;
		var autre =document.getElementById("champSelectMultipleAutre");
		
		if (nom != "")
		{
			strChoix = listeToString(choix);
	 		
	 		var txtarea = document.getElementById('formCode');
	 		var position = txtarea.selectionStart;
	 		var code = txtarea.value;
	 	
			debut = code.substr(0, position); 
			fin = code.substr(position); 
			
			if(autre.checked)
			{
				code = debut+id+"selectMultiple:!:"+nom+":!:"+strChoix+":!:autre:1:!:>\n"+fin;
			}
			else
			{
				code = debut+id+"selectMultiple:!:"+nom+":!:"+strChoix+":!:autre:0:!:>\n"+fin;
			}

			txtarea.value = code;
			
			$('#formSelectMultiple').modal('hide');
			
			document.getElementById('champSelectMultipleNom').value = "";
			document.getElementById('champSelectMultipleChoix').value = "";
		}
	}
	else if (type == "txtcourt")
	{
		var nom = document.getElementById('champTxtCourtNom').value;

		if (nom != "")
		{	 		
	 		var txtarea = document.getElementById('formCode');
	 		var position = txtarea.selectionStart;
	 		var code = txtarea.value;
	 	
			debut = code.substr(0, position); 
			fin = code.substr(position); 
			
			code = debut+id+"texteCourt:!:"+nom+":!:>\n"+fin;
			txtarea.value = code;
			
			$('#formTxtCourt').modal('hide');
			
			document.getElementById('champTxtCourtNom').value = "";
		}
	}
	else if (type == "txtlong")
	{
		var nom = document.getElementById('champTxtLongNom').value;

		if (nom != "")
		{	 		
	 		var txtarea = document.getElementById('formCode');
	 		var position = txtarea.selectionStart;
	 		var code = txtarea.value;
	 	
			debut = code.substr(0, position); 
			fin = code.substr(position); 
			
			code = debut+id+"texteLong:!:"+nom+":!:>\n"+fin;
			txtarea.value = code;
			
			$('#formTxtLong').modal('hide');
			
			document.getElementById('champTxtLongNom').value = "";
		}
	}
	else if (type == "fieldset")
	{
		var nom = document.getElementById('champFieldsetNom').value;

		if (nom != "")
		{	 		
	 		var txtarea = document.getElementById('formCode');
	 		var position = txtarea.selectionStart;
	 		var code = txtarea.value;
	 	
			debut = code.substr(0, position); 
			fin = code.substr(position); 
			
			code = debut+"<groupeQuestions:!:"+nom+":!:>\n<finGroupeQuestions:!:"+nom+":!:>\n"+fin;
			txtarea.value = code;
			
			$('#formFieldset').modal('hide');
			
			document.getElementById('champFieldsetNom').value = "";
		}
	}
	else if (type == "tableau")
	{
		var nom = document.getElementById('champTableauNom').value;
		var typeChamp = document.getElementById('champTableauType').value;
		var colonne = document.getElementById('champTableauColonne').value;
		var ligne = document.getElementById('champTableauLigne').value;
		var choix = document.getElementById('champTableauChoix').value;

		if (nom != "" && typeChamp != "none")
		{
			var strChoix = listeToString(choix);
			var strLigne = listeToStrLigne(ligne);
			var strColonne = listeToString(colonne);
			
	 		var txtarea = document.getElementById('formCode');
	 		var position = txtarea.selectionStart;
	 		var code = txtarea.value;
	 	
			debut = code.substr(0, position); 
			fin = code.substr(position); 
			
			code = debut+"<tableau:!:"+nom+":!:"+strLigne+":!:"+strColonne+":!:"+typeChamp+":!:"+strChoix+":!:>\n"+fin;
			txtarea.value = code;
			
			$('#formTableau').modal('hide');
			
			document.getElementById('champTableauNom').value = "";
			document.getElementById('champTableauType').value = "";
			document.getElementById('champTableauColonne').value = "";
			document.getElementById('champTableauLigne').value = "";
			document.getElementById('champTableauChoix').value = "";
		}
	}
}

function disableCheck(inputId)
{
	if (document.getElementById('check_'+inputId).checked)
	{
		document.getElementById('input_'+inputId).disabled = false;
	}
	else
	{
		document.getElementById('input_'+inputId).disabled = true;
	}
}

function disableSelect(inputId)
{
	var select = document.getElementById('select_'+inputId);
	
	var max = select.options.length;
	var autre = false;
	for (i=0 ; i< max ; i++)
	{
		if (select.options[i].selected && select.options[i].value == "sesaAutre")
		{
			autre = true;
		}
	}
	
	if (autre)
	{
		document.getElementById('input_'+inputId).disabled = false;
	}
	else
	{
		document.getElementById('input_'+inputId).disabled = true;
	}
}


function mdp(id)
{
	var mdp1 = document.getElementById('mdp1'+id).value;
	var mdp2 = document.getElementById('mdp2'+id).value;
	
	if (mdp1 != "" && mdp2 != "")
	{
		if (mdp1 == mdp2)
		{
			console.log('submit');
			document.getElementById('form'+id).submit();
		}
		else
		{
			document.getElementById('erreur'+id).innerHTML = '<div class="alert alert-danger">Vous devez entrer deux mots de passe identiques</div>';
		}
	}
}

function delForm(id)
{
	var rep = confirm("Voulez-vous vraiment supprimer ce questionnaire?");
	
	if (rep == true)
	{
		document.getElementById('suppr'+id).value = "true";
		document.getElementById('form'+id).submit();
	}
}
