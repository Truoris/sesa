-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 30 Août 2013 à 17:28
-- Version du serveur: 5.5.32
-- Version de PHP: 5.4.9-4ubuntu2.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `sesa`
--

-- --------------------------------------------------------

--
-- Structure de la table `sesa_79-test`
--

CREATE TABLE IF NOT EXISTS `sesa_79-test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` bigint(20) NOT NULL,
  `1_choix_multiple` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `2_choix_unique` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `3_select` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `4_select_multiple` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `6_txt_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `7_ligne1` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `8_ligne2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `5_txt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `17_ligne1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `18_ligne2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Contenu de la table `sesa_79-test`
--

INSERT INTO `sesa_79-test` (`id`, `date`, `1_choix_multiple`, `2_choix_unique`, `3_select`, `4_select_multiple`, `6_txt_long`, `7_ligne1`, `8_ligne2`, `5_txt`, `17_ligne1`, `18_ligne2`) VALUES
(1, 1377871594, 'None', 'None', 'None', 'None', 'None', 'None', 'None', '', '', ''),
(2, 1377871628, 'None', 'None', 'None', 'None', 'None', 'None', 'None', '', '', ''),
(3, 1377871740, 'None', 'None', 'None', 'None', 'None', 'None', 'None', '', '', ''),
(4, 1377871761, 'None', 'None', 'None', 'None', 'None', 'None', 'None', '', '', ''),
(5, 1377872151, 'None', 'None', 'None', 'None', 'None', 'None', 'None', '', '', ''),
(6, 1377872181, 'None', 'None', 'None', 'None', 'None', 'None', 'None', '', '', ''),
(7, 1377872223, 'None', 'None', 'None', 'None', 'None', 'None', 'None', '', '', ''),
(8, 1377872309, 'None', 'None', 'None', 'None', 'None', 'None', 'None', '', '', ''),
(9, 1377872319, 'None', 'None', 'None', 'None', 'None', 'None', 'None', '', '', ''),
(10, 1377872334, 'None', 'None', 'None', 'None', 'None', 'None', 'None', '', '', ''),
(11, 1377872792, '5::1', '2', '8', '1::7', 'titi', 'none::none', 'none::none', 'toto', 'none::none', 'none::none'),
(12, 1377872799, '5::1', '2', '8', '1::7', 'titi', 'none::none', 'none::none', 'toto', 'none::none', 'none::none'),
(13, 1377872831, '5::1', '2', '8', '1::7', 'titi', 'none::none', 'none::none', 'toto', 'none::none', 'none::none'),
(14, 1377872986, '1', '2', '3', '4', '6', '1::3', '2::1', '5', '10::12', '11::13'),
(15, 1377873673, 'none', 'none', '2', 'none', 'none', 'none::none', 'none::none', 'none', 'none::none', 'none::none');

-- --------------------------------------------------------

--
-- Structure de la table `sesa_80-tab`
--

CREATE TABLE IF NOT EXISTS `sesa_80-tab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `formId` int(11) NOT NULL,
  `date` bigint(20) NOT NULL,
  `1_ligne1` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `2_ligne2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `3_ligne1` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `4_ligne2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `sesa_81-john`
--

CREATE TABLE IF NOT EXISTS `sesa_81-john` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` bigint(20) NOT NULL,
  `1_bob` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `sesa_form_template`
--

CREATE TABLE IF NOT EXISTS `sesa_form_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `date` bigint(50) NOT NULL,
  `affichage` int(11) NOT NULL,
  `actif` int(11) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `template` longtext NOT NULL,
  `code` longtext NOT NULL,
  `champsNom` longtext NOT NULL,
  `slug` varchar(255) NOT NULL,
  `nbreChamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Contenu de la table `sesa_form_template`
--

INSERT INTO `sesa_form_template` (`id`, `userId`, `nom`, `date`, `affichage`, `actif`, `mdp`, `template`, `code`, `champsNom`, `slug`, `nbreChamp`) VALUES
(79, 0, 'Test', 1377865321, 0, 1, 'none', '$champs[0] = ''caseMultiple:1_choix_multiple:3'';\n$champs[1] = ''caseUnique:2_choix_unique'';\n$champs[2] = ''selectUnique:3_select'';\n$champs[3] = ''selectMultiple:4_select_multiple'';\n$champs[4] = ''texteCourt:5_txt'';\n$champs[5] = ''texteLong:6_txt_long'';\n$champs[6] = ''tabSelect:7_ligne1:2'';\n$champs[7] = ''tabSelect:8_ligne2:2'';\n$champs[8] = ''tabTexte:17_ligne1:2'';\n$champs[9] = ''tabTexte:18_ligne2:2'';\n', '<groupeQuestions:!:Classique:!:>\n<1_choix_multiple:!:caseMultiple:!:Choix multiple:!:1::2::3:!:autre:1:!:>\n<2_choix_unique:!:caseUnique:!:choix unique:!:1::2::3:!:autre:1:!:>\n<3_select:!:selectUnique:!:select:!:1::2::3:!:autre:1:!:>\n<4_select_multiple:!:selectMultiple:!:select multiple:!:1::2::3:!:autre:1:!:>\n<5_txt:!:texteCourt:!:txt:!:>\n<6_txt_long:!:texteLong:!:txt long:!:>\n<finGroupeQuestions:!:Classique:!:>\n<tableau:!:Tableau:!:7_ligne1:ligne1::8_ligne2:ligne2:!:colonne1::colonne2:!:selection:!:1::2::3:!:>\n<tableau:!:TabTexte:!:17_ligne1:ligne1::18_ligne2:ligne2:!:colonneA::colonneB:!:texteLibre:!::!:>\n', '', 'test', 9),
(80, 0, 'tab', 1377870866, 1, 1, 'none', '$champs[0] = ''tabSelect:1_ligne1:3'';\n$champs[1] = ''tabSelect:2_ligne2:3'';\n$champs[2] = ''tabTexte:3_ligne1:2'';\n$champs[3] = ''tabTexte:4_ligne2:2'';\n', '<tableau:!:Tableau:!:1_ligne1:ligne1::2_ligne2:ligne2:!:colonne1::colonne2::colonne3:!:selection:!:1::2::3:!:>\n<tableau:!:TabTexte:!:3_ligne1:ligne1::4_ligne2:ligne2:!:colonneA::colonneB:!:texteLibre:!::!:>\n', '', 'tab', 5),
(81, 0, 'john', 1377875962, 0, 0, 'none', '$champs[0] = ''caseUnique:1_bob'';\n', '<1_bob:!:caseUnique:!:bob:!:1::2::3:!:autre:1:!:>\n', '', 'john', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
