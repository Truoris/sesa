<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'questionnaire@home');
Route::post('/mdp-check.html', 'questionnaire@mdpCheck');
Route::any('/{id}-{slug}.html', 'questionnaire@fiche')->where('id', '[0-9]+');
Route::any('/admin', 'admin@home');
Route::get('/admin/new.html', 'admin@newQ');
Route::any('/admin/reponses/{id}-{slug}.html', 'reponses@liste')->where('id', '[0-9]+');
Route::any('/admin/edit-{id}-{slug}.html', 'admin@edit')->where('id', '[0-9]+');
Route::get('/admin/view-{slug}.html', 'admin@view');

Route::post('/admin/parser.html', 'admin@parser');
Route::post('/admin/save.html', 'admin@save');
