<?php
class admin extends BaseController 
{
	private function isIn($val, $tab)
	{
		$exp1 = explode(':', $val);
		foreach ($tab as $each)
		{
			$exp2 = explode(':', $each);
			if ($exp2[1] == $exp1[1])
			{
				return true;
			}
		}
		
		return false;
	}
	
	public function newQ()
	{
		return View::make('admin/newq');
	}
	
	public function view($slug)
	{
		return View::make('admin/fiche-template/'.$slug);
	}
	
	public function home()
	{
		if (Input::has('formId'))
		{
			if (Input::has('desactiver'))
			{
				formTemplate::where('id', '=', Input::get('formId'))->update(array('actif' => 0));
			}
			elseif (Input::has('activer'))
			{
				formTemplate::where('id', '=', Input::get('formId'))->update(array('actif' => 1));
			}
			elseif (Input::has('invisible'))
			{
				formTemplate::where('id', '=', Input::get('formId'))->update(array('affichage' => 0));
			}
			elseif (Input::has('visible'))
			{
				formTemplate::where('id', '=', Input::get('formId'))->update(array('affichage' => 1));
			}
			elseif (Input::has('suppr'))
			{
				formTemplate::where('id', '=', Input::get('formId'))->delete();
				Schema::drop(Input::get('formId').'-'.Input::get('formSlug'));
				unlink('../app/models/S'.Input::get('formId').Input::get('formSlug').'.php');
			}
			elseif (Input::has('mdp1'))
			{
				if (Input::get('mdp1') == Input::get('mdp2'))
				{
					$mdp = Hash::make(Input::get('mdp1'));
					formTemplate::where('id', '=', Input::get('formId'))->update(array('mdp' => $mdp));
				}
			}
			
			return Redirect::to('admin/');
		}
		
		$form = formTemplate::all();
		
		foreach ($form as $each)
		{
			$nomClass = "S".$each->id.preg_replace("(-)", '', $each->slug);
			$each->nbreRep = $nomClass::count();
			
			if ($each->mdp == "none")
			{
				$each->password = "Non";
			}
			else
			{
				$each->password = "Oui";
			}
		}
		
		$url = Config::get('app.url');
		
		return View::make('admin/home', array('form' => $form, 'url' => $url));
	}
	
	public function parser()
	{
		if (Input::has('code'))
		{
			$fichier = '<!DOCTYPE html>
				<html lang="fr" >
				<head>
					<meta charset="UTF-8">
					<title>SESA - Home</title>
					<meta name="viewport" content="width=device-width, initial-scale=1.0">
					<meta name="description" content="Web application for online survey" />
					<base href="http://127.0.0.1/sesa/public/">
					<link href="static/css/bootstrap.css" rel="stylesheet">
					<link href="static/css/bootstrap-responsive.css" rel="stylesheet">
					<link href="static/css/design.css" rel="stylesheet">
				</head>
				<body>
				<div class="navbar navbar-inverse navbar-fixed-top">
					<div class="navbar-inner">
					<div class="container">
					<a class="brand">SESA - Questionnaire</a>

					<ul class="nav pull-right">
					<li><a>Retour à l\'accueil</a></li>
					</ul>
					</div>
					</div>
				</div>
				
				<div class="space"></div>
				
				<div style="margin-left: 10px;margin-right: 10px;">
				<h1>Questionnaire '.Input::get('nom').'</h1>';
			
			$code = preg_replace("(\r\n|\n|\r)", ':$:', Input::get('code'));
			$code = explode(':$:', $code);
			
			$name = 1;
			$inputAutre = 1;
			
			foreach ($code as $ligne)
			{
				if ($ligne != "")
				{
					$exp = explode('<', $ligne);
					$args = explode(':!:', $exp[1]);
					
					if ($args[1] == "caseMultiple")
					{
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;">'.$args[2].' :<br \>';
						
						$choix = explode('::', $args[3]);
						
						foreach ($choix as $each)
						{
							$html .= ' <label class="checkbox inline"><input type="checkbox"> '.$each.'</label>';
						}
						
						if ($args[4] == "autre:1")
						{
							$html .= ' <label class="checkbox inline"><input id="check_'.$inputAutre.'" type="checkbox" onclick="disableCheck(\''.$inputAutre.'\')"> Autre :</label> <input id="input_'.$inputAutre.'" style="margin-top: 15px;margin-left: 5px;" type="text" disabled>';
							$inputAutre++;
						}
						
						$html .= '</div>';
						
						$fichier .= $html."\n";
					}
					elseif ($args[1] == "caseUnique")
					{
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;">'.$args[2].' :<br \>';
						
						$choix = explode('::', $args[3]);
						
						foreach ($choix as $each)
						{
							$html .= ' <label class="radio inline"><input type="radio" name="'.$name.'" onclick="disableCheck(\''.$inputAutre.'\')"> '.$each.'</label>';
						}
						
						if ($args[4] == "autre:1")
						{
							$html .= ' <label class="radio inline"><input id="check_'.$inputAutre.'" type="radio" name="'.$name.'" onclick="disableCheck(\''.$inputAutre.'\')"> Autre :</label> <input id="input_'.$inputAutre.'" style="margin-top: 15px;margin-left: 5px;" type="text" disabled>';
							$inputAutre++;
						}
						
						$name++;
						
						$html .= '</div>';
						
						$fichier .= $html."\n";
					}
					elseif ($args[1] == "selectUnique")
					{
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;" class="input-prepend"><span class="add-on">'.$args[2].'</span><select id="select_'.$inputAutre.'"><option> --- </option>';
						
						$choix = explode('::', $args[3]);
						
						foreach ($choix as $each)
						{
							$html .= '<option onclick="disableSelect(\''.$inputAutre.'\')">'.$each.'</option>';
						}
						
						if ($args[4] == "autre:1")
						{
							$html .= '<option onclick="disableSelect(\''.$inputAutre.'\')" value="sesaAutre">Autre</option>';
						}
						
						$html .= '</select></div>';
						
						if ($args[4] == "autre:1")
						{
							$html .= '<div>Autre : <input id="input_'.$inputAutre.'" style="margin-top: 15px;margin-left: 5px;" type="text" disabled></div>';
							$inputAutre++;
						}
						
						$fichier .= $html."\n";
					}
					elseif ($args[1] == "selectMultiple")
					{
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;">'.$args[2].' :<br \><select id="select_'.$inputAutre.'" multiple="multiple">';
						
						$choix = explode('::', $args[3]);
						
						foreach ($choix as $each)
						{
							$html .= '<option onclick="disableSelect(\''.$inputAutre.'\')">'.$each.'</option>';
						}
						
						if ($args[4] == "autre:1")
						{
							$html .= '<option onclick="disableSelect(\''.$inputAutre.'\')" value="sesaAutre">Autre</option>';
						}
						
						$html .= '</select></div>';
						
						if ($args[4] == "autre:1")
						{
							$html .= '<div>Autre : <input id="input_'.$inputAutre.'" style="margin-top: 15px;margin-left: 5px;" type="text" disabled></div>';
							$inputAutre++;
						}
						
						$fichier .= $html."\n";
					}
					elseif ($args[1] == "texteCourt")
					{
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;" class="input-prepend"><span class="add-on">'.$args[2].'</span><input type="text"></div>';
						
						$fichier .= $html."\n";
					}
					elseif ($args[1] == "texteLong")
					{
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;"> <span style="margin-left: 20px;">'.$args[2].' :</span><br \><textarea style="width: 400px;height: 100px;"></textarea></div>';
						
						$fichier .= $html."\n";
					}
					elseif ($args[0] == "groupeQuestions")
					{
						$html = '<fieldset><legend style="margin-bottom: 0px;">'.$args[1].'</legend>';
						
						$fichier .= $html."\n";
					}
					elseif ($args[0] == "finGroupeQuestions")
					{
						$html = '</fieldset>';
						
						$fichier .= $html."\n";
					}
					elseif ($args[0] == "tableau")
					{
						$html = '<table class="table table-bordered" style="margin-top: 20px;"><thead><tr><th>'.$args[1].'</th>';
						
						$colones = explode('::', $args[3]);
						$nbreColone = 0;
						
						foreach ($colones as $each)
						{
							if ($each != "")
							{
								$nbreColone++;
								$html .= '<th>'.$each.'</th>';
							}
						}
						
						$html .= '</tr></thead><tbody>';
						
						$lignes = explode('::', $args[2]);
						
						foreach ($lignes as $each)
						{
							if ($each != "")
							{
								$info = explode(':', $each);
								$html .= '<tr><td>'.$info[1].'</td>';
								
								for ($i=1; $i<=$nbreColone ; $i++)
								{
									if ($args[4] == "texteLibre")
									{
										$html .= '<td><input type="text" class="input-medium"></td>';
									}
									elseif ($args[4] == "selection")
									{
										$html .= '<td><select><option> --- </option>';
										
										$listeChoix = explode('::', $args[5]);
										foreach ($listeChoix as $choix)
										{
											$html .= '<option>'.$choix.'</option>';
										}
										
										$html .= '</select></td>';
									}
								}
								$html .= '</tr>';
							}
						}
						
						$html .= '</tbody></table>';
						
						$fichier .= $html."\n";
					}
				}
			}
			
			$fichier .= '<div style="margin-left: 30%;margin-top: 15px;margin-bottom: 15px;"><input type="submit" value="Enregistrer" class="btn btn-primary"></div></div><script type="text/javascript" src="static/js/fonction.js"></script></body></html>';
			
			$slug = Str::slug(Input::get('nom'));
			
			$file = fopen('../app/views/admin/fiche-template/'.$slug.'.twig', 'w');
			
			fwrite($file, $fichier);
			
			fclose($file);
			
			return Response::make("::::::".$slug."::::::");
		}
		
		return Response::make(0);
	}
	
	public function save()
	{
		if (Input::has('code') and Input::has('nom'))
		{
			$fichier = '<!DOCTYPE html>
				<html lang="fr" >
				<head>
					<meta charset="UTF-8">
					<title>SESA - Home</title>
					<meta name="viewport" content="width=device-width, initial-scale=1.0">
					<meta name="description" content="Web application for online survey" />
					<base href="http://127.0.0.1/sesa/public/">
					<link href="static/css/bootstrap.css" rel="stylesheet">
					<link href="static/css/bootstrap-responsive.css" rel="stylesheet">
					<link href="static/css/design.css" rel="stylesheet">
				</head>
				<body>
				<div class="navbar navbar-inverse navbar-fixed-top">
					<div class="navbar-inner">
					<div class="container">
					<a class="brand">SESA - Questionnaire</a>

					<ul class="nav pull-right">
					<li><a href="">Retour à l\'accueil</a></li>
					</ul>
					</div>
					</div>
				</div>
				
				<div style="height: 50px;"></div>
				
				{% if mdp == true %}
					<div id="page" class="container hide">
				{% else %}
					<div class="container">
				{% endif %}
				<form method="post" action="">
				<h2 style="text-align: center;">Questionnaire '.Input::get('nom').'</h2>';
			
			$code = preg_replace("(\r\n|\n|\r)", ':$:', Input::get('code'));
			$code = explode(':$:', $code);
			
			$nbreChamp = 1;
			$formTemplate = "";
			$strCode = "";
			$h = 0;
			
			foreach ($code as $ligne)
			{
				if ($ligne != "")
				{
					$exp = explode('<', $ligne);
					$args = explode(':!:', $exp[1]);
					
					if ($args[1] == "caseMultiple")
					{
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;">'.$args[2].' :<br \>';
						
						$choix = explode('::', $args[3]);
						
						$offset = 0;
						$input = "";
						
						$nomChamp = $nbreChamp."_".preg_replace('(-)', '_', Str::slug($args[2]));
						
						$strCode .= "<".$nomChamp.":!:";
						
						$max = count($args);

						foreach ($args as $key => $info)
						{
							if ($info != "ID")
							{
								if ($key < $max-1)
								{
									$strCode .= $info.":!:";
								}
								else
								{
									$strCode .= $info;
								}
							}
						}
						
						$strCode .= "\n";
						
						foreach ($choix as $each)
						{
							$offset++;
							$html .= ' <label class="checkbox inline"><input name="'.$nomChamp.'_'.$offset.'" value="'.$each.'" type="checkbox"> '.$each.'</label>';
						}
						
						if ($args[4] == "autre:1")
						{
							if ($offset == 0)
							{
								$input .= "Input::get('".$nomChamp."_0')";
							}
							else
							{
								$input .= ".':!:'.Input::get('".$nomChamp."_0')";
							}
							$html .= ' <label class="checkbox inline"><input id="'.$nomChamp.'_0" name="'.$nomChamp.'_0" type="checkbox" value="sesaAutre" onclick="disableCheck(\''.$nomChamp.'\')"> Autre :</label> <input name="'.$nomChamp.'_autre" id="'.$nomChamp.'_autre" style="margin-top: 15px;margin-left: 5px;" type="text" disabled>';
						}
						
						$html .= '</div>';
						
						$formTemplate .= "\$champs[".$h."] = 'caseMultiple:".$nomChamp.":".$offset."';\n";
						$h++;
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[1] == "caseUnique")
					{
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;">'.$args[2].' :<br \>';
						
						$choix = explode('::', $args[3]);
						
						$nomChamp = $nbreChamp."_".preg_replace("(-)", '_', Str::slug($args[2]));
						
						$strCode .= "<".$nomChamp.":!:";
						
						$max = count($args);

						foreach ($args as $key => $info)
						{
							if ($info != "ID")
							{
								if ($key < $max-1)
								{
									$strCode .= $info.":!:";
								}
								else
								{
									$strCode .= $info;
								}
							}
						}
						
						$strCode .= "\n";
						
						foreach ($choix as $each)
						{
							$html .= ' <label class="radio inline"><input name="'.$nomChamp.'" type="radio" value="'.$each.'" onclick="disableCheck(\''.$nomChamp.'\')"> '.$each.'</label>';
						}
						
						if ($args[4] == "autre:1")
						{
							$html .= ' <label class="radio inline"><input name="'.$nomChamp.'" id="'.$nomChamp.'_0"  type="radio" value="sesaAutre" onclick="disableCheck(\''.$nomChamp.'\')"> Autre :</label> <input name="'.$nomChamp.'_autre" id="'.$nomChamp.'_autre" style="margin-top: 15px;margin-left: 5px;" type="text" disabled>';
						}
						
						$html .= '</div>';
						
						$formTemplate .= "\$champs[".$h."] = 'caseUnique:".$nomChamp."';\n";
						$h++;
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[1] == "selectUnique")
					{
						$nomChamp = $nbreChamp."_".preg_replace("(-)", '_', Str::slug($args[2]));
						
						$strCode .= "<".$nomChamp.":!:";
						
						$max = count($args);

						foreach ($args as $key => $info)
						{
							if ($info != "ID")
							{
								if ($key < $max-1)
								{
									$strCode .= $info.":!:";
								}
								else
								{
									$strCode .= $info;
								}
							}
						}
						
						$strCode .= "\n";
						
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;" class="input-prepend"><span class="add-on">'.$args[2].'</span><select id="'.$nomChamp.'" name="'.$nomChamp.'"><option value="none"> --- </option>';
						
						$choix = explode('::', $args[3]);
						
						foreach ($choix as $each)
						{
							$html .= '<option onclick="disableSelect(\''.$nomChamp.'\')" value="'.$each.'">'.$each.'</option>';
						}
						
						if ($args[4] == "autre:1")
						{
							$html .= '<option onclick="disableSelect(\''.$nomChamp.'\')" value="sesaAutre">Autre</option>';
						}
						
						$html .= '</select></div>';
					
						if ($args[4] == "autre:1")
						{
							$html .= '<div>Autre : <input name="'.$nomChamp.'_autre" id="'.$nomChamp.'_autre" style="margin-top: 15px;margin-left: 5px;" type="text" disabled></div>';
						}
						
						$formTemplate .= "\$champs[".$h."] = 'selectUnique:".$nomChamp."';\n";
						$h++;
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[1] == "selectMultiple")
					{
						$nomChamp = $nbreChamp."_".preg_replace("(-)", '_', Str::slug($args[2]));
						
						$strCode .= "<".$nomChamp.":!:";
						
						$max = count($args);

						foreach ($args as $key => $info)
						{
							if ($info != "ID")
							{
								if ($key < $max-1)
								{
									$strCode .= $info.":!:";
								}
								else
								{
									$strCode .= $info;
								}
							}
						}
						
						$strCode .= "\n";
						
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;">'.$args[2].' :<br \><select id="'.$nomChamp.'" name="'.$nomChamp.'" multiple="multiple">';
						
						$choix = explode('::', $args[3]);
						
						foreach ($choix as $each)
						{
							$html .= '<option onclick="disableSelect(\''.$nomChamp.'\')" value="'.$each.'">'.$each.'</option>';
						}
						
						if ($args[4] == "autre:1")
						{
							$html .= '<option onclick="disableSelect(\''.$nomChamp.'\')" value="sesaAutre">Autre</option>';
						}
						
						$html .= '</select></div>';
						
						if ($args[4] == "autre:1")
						{
							$html .= '<div>Autre : <input name="'.$nomChamp.'_autre" id="'.$nomChamp.'_autre" style="margin-top: 15px;margin-left: 5px;" type="text" disabled></div>';
						}
						
						$formTemplate .= "\$champs[".$h."] = 'selectMultiple:".$nomChamp."';\n";
						$h++;
												
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[1] == "texteCourt")
					{
						$nomChamp = $nbreChamp."_".preg_replace("(-)", '_', Str::slug($args[2]));
						
						$strCode .= "<".$nomChamp.":!:";
						
						$max = count($args);

						foreach ($args as $key => $info)
						{
							if ($info != "ID")
							{
								if ($key < $max-1)
								{
									$strCode .= $info.":!:";
								}
								else
								{
									$strCode .= $info;
								}
							}
						}
						
						$strCode .= "\n";
						
						$formTemplate .= "\$champs[".$h."] = 'texteCourt:".$nomChamp."';\n";
						$h++;
						
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;" class="input-prepend"><span class="add-on">'.$args[2].'</span><input name="'.$nomChamp.'" type="text"></div>';
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[1] == "texteLong")
					{
						$nomChamp = $nbreChamp."_".preg_replace("(-)", '_', Str::slug($args[2]));
						
						$strCode .= "<".$nomChamp.":!:";
						
						$max = count($args);

						foreach ($args as $key => $info)
						{
							if ($info != "ID")
							{
								if ($key < $max-1)
								{
									$strCode .= $info.":!:";
								}
								else
								{
									$strCode .= $info;
								}
							}
						}
						
						$strCode .= "\n";
						
						$formTemplate .= "\$champs[".$h."] = 'texteLong:".$nomChamp."';\n";
						$h++;
						
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;"> <span style="margin-left: 20px;">'.$args[2].' :</span><br \><textarea name="'.$nomChamp.'" style="width: 400px;height: 100px;" maxlength="255"></textarea></div>';
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[0] == "groupeQuestions")
					{
						$html = '<fieldset><legend style="margin-bottom: 0px;">'.$args[1].'</legend>';
						
						$strCode .= $ligne."\n";
						
						$fichier .= $html."\n";
						
					}
					elseif ($args[0] == "finGroupeQuestions")
					{
						$strCode .= $ligne."\n";
						$html = '</fieldset>';
						
						$fichier .= $html."\n";
					}
					elseif ($args[0] == "tableau")
					{
						$html = '<table class="table table-bordered" style="margin-top: 20px;"><thead><tr><th>'.$args[1].'</th>';
						
						$colones = explode('::', $args[3]);
						$nbreColone = 0;
						
						$strCode .= "<tableau:!:".$args[1].":!:";
						
						foreach ($colones as $each)
						{
							if ($each != "")
							{
								$nbreColone++;
								$html .= '<th>'.$each.'</th>';
							}
						}
						
						$html .= '</tr></thead><tbody>';
						
						$lignes = explode('::', $args[2]);
						
						$nbreLigne = 0;
						
						foreach ($lignes as $each)
						{
							if ($each != "")
							{
								$offset = 0;
								
								$info = explode(':', $each);
								
								$html .= '<tr><td>'.$info[1].'</td>';
								
								$nomChamp = $nbreChamp."_".preg_replace("(-)", '_', Str::slug($info[1]));
								
								if ($nbreLigne == 0)
								{
									$strCode .= $nomChamp.":".$info[1];
									$nbreLigne++;
								}
								else
								{
									$strCode .= "::".$nomChamp.":".$info[1];
								}
								
								
								for ($i=1; $i<=$nbreColone ; $i++)
								{
									if ($args[4] == "texteLibre")
									{
										$offset++;
										$html .= '<td><input type="text" name="'.$nomChamp.'_'.$offset.'" class="input-medium"></td>';
									}
									elseif ($args[4] == "selection")
									{
										$offset++;
										
										$html .= '<td><select name="'.$nomChamp.'_'.$offset.'"><option value="none"> --- </option>';
										
										$listeChoix = explode('::', $args[5]);
										foreach ($listeChoix as $choix)
										{
											$html .= '<option value="'.$choix.'">'.$choix.'</option>';
										}
										
										$html .= '</select></td>';
									}
								}
								
								if ($args[4] == "texteLibre")
								{
									$formTemplate .= "\$champs[".$h."] = 'tabTexte:".$nomChamp.":".$offset."';\n";
								}
								elseif ($args[4] == "selection")
								{
									$formTemplate .= "\$champs[".$h."] = 'tabSelect:".$nomChamp.":".$offset."';\n";
								}
								
								$h++;
								$nbreChamp++;
								
								$html .= '</tr>';
							}
						}
						
						$strCode .= ":!:".$args[3].":!:".$args[4].":!:".$args[5].":!:>\n";
						
						$html .= '</tbody></table>';
						
						$fichier .= $html."\n";
					}
				}
			}
			
			$fichier .= '<div style="margin-left: 30%;margin-top: 15px;margin-bottom: 15px;"><input name="envoi" class="btn btn-primary" type="submit" value="Enregistrer"></div></form></div><script type="text/javascript" src="static/js/public-fonction.js"></script>{% if mdp == true %}<script>mdp("{{ id }}");</script>{% endif %}</body></html>';
			
			$slug = Str::slug(Input::get('nom'));
			
			$template = new formTemplate;
			
			$template->userId = 0;
			$template->nom = Input::get('nom');
			$template->date = time();
			$template->affichage = 0;
			$template->actif = 0;
			$template->mdp = "none";
			$template->template = $formTemplate;
			$template->code = $strCode;
			$template->slug = $slug;
			$template->nbreChamp = $nbreChamp--;
			
			$template->save();
			
			Schema::create($template->id.'-'.$slug, function($table) use ($formTemplate)
			{
				$table->increments('id');
				$table->bigInteger('date');
				
				$var = eval($formTemplate);
				
				foreach ($champs as $ligne)
				{
					$info = explode(':', $ligne);
					$table->string($info[1], 255)->default('none');
				}
			});
			
			$nomClass = "S".$template->id.preg_replace("(-)", '', $slug);
			
			$file = fopen('../app/models/'.$nomClass.'.php', 'w');
			
			$model = "<?php\nclass ".$nomClass." extends Eloquent {\nprotected \$table = '".$template->id.'-'.$slug."';\npublic \$timestamps = false;\n}\n?>";
			
			fwrite($file, $model);
			
			fclose($file);
			
			$file = fopen('../app/views/fiche-template/'.$template->id.'-'.$slug.'.twig', 'w');
			
			fwrite($file, $fichier);
			
			fclose($file);
			
			return Response::make("::::::".$slug."::::::");
		}
		
		return Response::make(0);
	}
	
	public function edit($id, $slug)
	{
		if (Input::has('code') and Input::has('nom'))
		{
			$fichier = '<!DOCTYPE html>
				<html lang="fr" >
				<head>
					<meta charset="UTF-8">
					<title>SESA - Home</title>
					<meta name="viewport" content="width=device-width, initial-scale=1.0">
					<meta name="description" content="Web application for online survey" />
					<base href="http://127.0.0.1/sesa/public/">
					<link href="static/css/bootstrap.css" rel="stylesheet">
					<link href="static/css/bootstrap-responsive.css" rel="stylesheet">
					<link href="static/css/design.css" rel="stylesheet">
				</head>
				<body>
				<div class="navbar navbar-inverse navbar-fixed-top">
					<div class="navbar-inner">
					<div class="container">
					<a class="brand">SESA - Questionnaire</a>

					<ul class="nav pull-right">
					<li><a href="">Retour à l\'accueil</a></li>
					</ul>
					</div>
					</div>
				</div>
				
				<div style="height: 50px;"></div>
				
				{% if mdp == true %}
					<div id="page" class="container hide">
				{% else %}
					<div class="container">
				{% endif %}
				<form method="post" action="">
				<h2 style="text-align: center;">Questionnaire '.Input::get('nom').'</h2>';
			
			$code = preg_replace("(\r\n|\n|\r)", ':$:', Input::get('code'));
			$code = explode(':$:', $code);
			
			$template = formTemplate::where('id', '=', $id)->first();
			
			$nbreChamp = $template->nbreChamp;
			$formTemplate = "";
			$strCode = "";
			$h = 0;
			
			foreach ($code as $ligne)
			{
				if ($ligne != "")
				{
					$exp = explode('<', $ligne);
					$args = explode(':!:', $exp[1]);
					
					if ($args[0] == "ID")
					{
						
						$nbreChamp++;
						$args[0] = $nbreChamp."_".preg_replace("(-)", '_', Str::slug($args[2]));
					}
					
					if ($args[1] == "caseMultiple")
					{
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;">'.$args[2].' :<br \>';
						
						$choix = explode('::', $args[3]);
						
						$offset = 0;
						$input = "";
						
						$nomChamp = $args[0];
						
						foreach ($choix as $each)
						{
							$offset++;
							$html .= ' <label class="checkbox inline"><input name="'.$nomChamp.'_'.$offset.'" value="'.$each.'" type="checkbox"> '.$each.'</label>';
						}
						
						if ($args[4] == "autre:1")
						{
							$html .= ' <label class="checkbox inline"><input id="'.$nomChamp.'_0" name="'.$nomChamp.'_0" type="checkbox" value="sesaAutre" onclick="disableCheck(\''.$nomChamp.'\')"> Autre :</label> <input name="'.$nomChamp.'_autre" id="'.$nomChamp.'_autre" style="margin-top: 15px;margin-left: 5px;" type="text" disabled>';
						}
						
						$html .= '</div>';
						
						$formTemplate .= "\$champs[".$h."] = 'caseMultiple:".$nomChamp.":".$offset."';\n";
						$h++;
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[1] == "caseUnique")
					{
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;">'.$args[1].' :<br \>';
						
						$choix = explode('::', $args[3]);
						
						$nomChamp = $args[0];
						
						foreach ($choix as $each)
						{
							$html .= ' <label class="radio inline"><input name="'.$nomChamp.'" type="radio" value="'.$each.'" onclick="disableCheck(\''.$nomChamp.'\')"> '.$each.'</label>';
						}
						
						if ($args[4] == "autre:1")
						{
							$html .= ' <label class="radio inline"><input id="'.$nomChamp.'_0" name="'.$nomChamp.'" type="radio" value="sesaAutre" onclick="disableCheck(\''.$nomChamp.'\')"> Autre :</label> <input name="'.$nomChamp.'_autre" id="'.$nomChamp.'_autre" style="margin-top: 15px;margin-left: 5px;" type="text" disabled>';
						}
						
						$html .= '</div>';
						
						$formTemplate .= "\$champs[".$h."] = 'caseUnique:".$nomChamp."';\n";
						$h++;
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[1] == "selectUnique")
					{
						$nomChamp = $args[0];
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;" class="input-prepend"><span class="add-on">'.$args[2].'</span><select id="'.$nomChamp.'" name="'.$nomChamp.'"><option value="none"> --- </option>';
						
						$choix = explode('::', $args[3]);
						
						foreach ($choix as $each)
						{
							$html .= '<option onclick="disableSelect(\''.$nomChamp.'\')" value="'.$each.'">'.$each.'</option>';
						}
						
						if ($args[4] == "autre:1")
						{
							$html .= '<option onclick="disableSelect(\''.$nomChamp.'\')" value="sesaAutre">Autre</option>';
						}
						
						$html .= '</select></div>';
						
						if ($args[4] == "autre:1")
						{
							$html .= '<div>Autre : <input name="'.$nomChamp.'_autre" id="'.$nomChamp.'_autre" style="margin-top: 15px;margin-left: 5px;" type="text" disabled></div>';
						}
						
						$formTemplate .= "\$champs[".$h."] = 'selectUnique:".$nomChamp."';\n";
						$h++;
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[1] == "selectMultiple")
					{
						$nomChamp = $args[0];
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;">'.$args[2].' :<br \><select id="'.$nomChamp.'" name="'.$nomChamp.'[]" multiple="multiple">';
						
						$choix = explode('::', $args[3]);
						
						foreach ($choix as $each)
						{
							$html .= '<option onclick="disableSelect(\''.$nomChamp.'\')" value="'.$each.'">'.$each.'</option>';
						}
						
						if ($args[4] == "autre:1")
						{
							$html .= '<option onclick="disableSelect(\''.$nomChamp.'\')" value="sesaAutre">Autre</option>';
						}
						
						$html .= '</select></div>';
						
						if ($args[4] == "autre:1")
						{
							$html .= '<div>Autre : <input name="'.$nomChamp.'_autre" id="'.$nomChamp.'_autre" style="margin-top: 15px;margin-left: 5px;" type="text" disabled></div>';
						}
						
						$formTemplate .= "\$champs[".$h."] = 'selectMultiple:".$nomChamp."';\n";
						$h++;
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[1] == "texteCourt")
					{
						$nomChamp = $args[0];
						$formTemplate .= "\$champs[".$h."] = 'texteCourt:".$nomChamp."';\n";
						$h++;
						
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;" class="input-prepend"><span class="add-on">'.$args[1].'</span><input name="'.$nomChamp.'" type="text"></div>';
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[1] == "texteMultiple")
					{
						$nomChamp = $args[0];
						$formTemplate .= "\$champs[".$h."] = 'texteMultiple:".$nomChamp."';\n";
						$h++;
						
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;">'.$args[2].' :<br \>1 : <input name="'.$nomChamp.'_1" type="text"><br \>2 : <input name="'.$nomChamp.'_2" type="text"> <div style="margin-left: 30px;"><i class="icon-plus"></i> Ajouter une autre réponse</div></div>';
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[1] == "texteLong")
					{
						$nomChamp = $args[0];
						$formTemplate .= "\$champs[".$h."] = 'texteLong:".$nomChamp."';\n";
						$h++;
						
						$html = '<div style="width: 80%;margin-auto;margin-top: 20px;padding: 0;"> <span style="margin-left: 20px;">'.$args[2].' :</span><br \><textarea name="'.$nomChamp.'" style="width: 400px;height: 100px;" maxlength="255"></textarea></div>';
						
						$fichier .= $html."\n";
						$nbreChamp++;
					}
					elseif ($args[0] == "groupeQuestions")
					{
						$html = '<fieldset><legend style="margin-bottom: 0px;">'.$args[1].'</legend>';
						
						$fichier .= $html."\n";
						
					}
					elseif ($args[0] == "finGroupeQuestions")
					{
						$html = '</fieldset>';
						
						$fichier .= $html."\n";
					}
					elseif ($args[0] == "tableau")
					{
						$html = '<table class="table table-bordered" style="margin-top: 20px;"><thead><tr><th>'.$args[1].'</th>';
						
						$strCode .= "<tableau:!:".$args[1].":!:";
						
						$colones = explode('::', $args[3]);
						$nbreColone = 0;
						
						foreach ($colones as $each)
						{
							if ($each != "")
							{
								$nbreColone++;
								$html .= '<th>'.$each.'</th>';
							}
						}
						
						$html .= '</tr></thead><tbody>';
						
						$lignes = explode('::', $args[2]);
						$nbreLigne = 0;
						
						foreach ($lignes as $each)
						{
							if ($each != "")
							{
								$offset = 0;
								
								$info = explode(':', $each);
								
								if ($info[0] == "ID")
								{
									$info[0] = $nbreChamp."_".preg_replace("(-)", '_', Str::slug($info[1]));
								}
								
								$nomChamp = $info[0];
								
								if ($nbreLigne == 0)
								{
									$strCode .= $nomChamp.":".$info[1];
									$nbreLigne++;
								}
								else
								{
									$strCode .= "::".$nomChamp.":".$info[1];
								}
								
								$html .= '<tr><td>'.$info[1].'</td>';
								
								for ($i=1; $i<=$nbreColone ; $i++)
								{
									if ($args[4] == "texteLibre")
									{
										$offset++;
										$html .= '<td><input type="text" name="'.$nomChamp.'_'.$offset.'" class="input-medium"></td>';
									}
									elseif ($args[4] == "selection")
									{
										$offset++;
										
										$html .= '<td><select name="'.$nomChamp.'_'.$offset.'"><option value="none"> --- </option>';
										
										$listeChoix = explode('::', $args[5]);
										foreach ($listeChoix as $choix)
										{
											$html .= '<option value="'.$choix.'">'.$choix.'</option>';
										}
										
										$html .= '</select></td>';
									}
								}
								
								if ($args[4] == "texteLibre")
								{
									$formTemplate .= "\$champs[".$h."] = 'tabTexte:".$nomChamp.":".$offset."';\n";
								}
								elseif ($args[4] == "selection")
								{
									$formTemplate .= "\$champs[".$h."] = 'tabSelect:".$nomChamp.":".$offset."';\n";
								}
								
								$h++;
								$nbreChamp++;
								
								$html .= '</tr>';
							}
						}
						
						$strCode .= ":!:".$args[3].":!:".$args[4].":!:".$args[5].":!:>\n";
						
						$html .= '</tbody></table>';
						
						$fichier .= $html."\n";
					}
					
					if ($args[0] != "tableau")
					{
						if (isset($nomChamp))
						{
							$strCode .= "<".$nomChamp.":!:";
							
							$max = count($args);

							foreach ($args as $key => $info)
							{
								if ($info != "ID" and $info != $nomChamp)
								{
									if ($key < $max-1)
									{
										$strCode .= $info.":!:";
									}
									else
									{
										$strCode .= $info;
									}
								}
							}
							
							$strCode .= "\n";
						}
						else
						{
							$strCode .= $ligne."\n";
						}
					}
					
					unset($nomChamp);
				}
			}
			
			$fichier .= '<div style="margin-left: 30%;margin-top: 15px;margin-bottom: 15px;"><input name="envoi" class="btn btn-primary" type="submit" value="Enregistrer"></div></form></div><script type="text/javascript" src="static/js/public-fonction.js"></script>{% if mdp == true %}<script>mdp("{{ id }}");</script>{% endif %}</body></html>';
			
			$oldTemplate = $template->template;
			$i = 0;
			
			$colonneDel = array();
			$colonneAdd = array();
			
			eval($oldTemplate);
			
			foreach ($champs as $each)
			{
				$oldChamps[$i] = $each;
				$i++;
			}
			
			$champs = array();
			
			eval($formTemplate);
			
			$i = 0;
			
			foreach ($champs as $each)
			{
				if (!$this->isIn($each, $oldChamps))
				{
					$info = explode(':', $each);
					$colonneAdd[$i] = $info[1];
					$i++;
				}
			}
			
			$i = 0;
			
			foreach ($oldChamps as $each)
			{
				if (!$this->isIn($each, $champs))
				{
					$info = explode(':', $each);
					$colonneDel[$i] = $info[1];
					$i++;
				}
			}
			
			Schema::table($template->id.'-'.$template->slug, function($table) use ($colonneDel)
			{
				foreach ($colonneDel as $each)
				{
					$table->dropColumn($each);
				}
			});
			
			Schema::table($template->id.'-'.$template->slug, function($table) use ($colonneAdd)
			{
				foreach ($colonneAdd as $each)
				{
					$table->string($each, 255);
				}
			});
    
			$template->nom = Input::get('nom');
			$template->template = $formTemplate;
			$template->code = $strCode;
			
			$template->save();	
			
			$file = fopen('../app/views/fiche-template/'.$template->id.'-'.$template->slug.'.twig', 'w');
			
			fwrite($file, $fichier);
			
			fclose($file);
			
			return Response::make("::::::".$slug."::::::");
		}
		else
		{
			$form = formTemplate::where('id', '=', $id)->first();;
		
			return View::make('admin/newq', array('form' => $form));
		}
	}
}
	
