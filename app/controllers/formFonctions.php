<?php

function caseMultiple($id, $offset)
{
	$return = "";
	$h = 0;
	
	for ($i=0 ; $i<=$offset ; $i++)
	{
		if (Input::has($id.'_'.$i) and $i != 0)
		{
			if ($h == 0)
			{
				$return .= Input::get($id."_".$i);
				$h++;
			}
			else
			{
				$return .= "::".Input::get($id."_".$i)."";
			}
		}
		elseif (Input::has($id.'_'.$i) and $i == 0)
		{
			if (Input::has($id."_autre"))
			{
				$autre = Input::get($id."_autre");
			}	
			else
			{
				$autre = 'none';
			}
			
			if ($h == 0)
			{
				$return .= $autre;
				$h++;
			}
			else
			{
				$return .= "::".$autre."";
			}
		}
	}
	
	if ($return == "")
	{
		return "\$formRep['".$id."'] = 'none';\n";
	}
	else
	{
		$return = "\$formRep['".$id."'] = '".$return."';\n";
		return $return;
	}
}

function caseUnique($id, $offset)
{
	if (Input::has($id) and Input::get($id) != "sesaAutre")
	{
		return "\$formRep['".$id."'] = ".Input::get($id).";\n";
	}
	elseif (Input::has($id) and Input::get($id) == "sesaAutre")
	{
		if (Input::has($id."_autre"))
		{
			$autre = Input::get($id."_autre");
		}	
		else
		{
			$autre = 'none';
		}
			
		return "\$formRep['".$id."'] = ".$autre.";\n";
	}
	else
	{
		return "\$formRep['".$id."'] = 'none';\n";
	}
}

function selectUnique($id, $offset)
{
	if (Input::has($id) and Input::get($id) != "sesaAutre")
	{
		return "\$formRep['".$id."'] = '".Input::get($id)."';\n";
	}
	elseif (Input::has($id) and Input::get($id) == "sesaAutre")
	{
		return "\$formRep['".$id."'] = '".Input::get($id."_autre")."';\n";
	}
	else
	{
		return "\$formRep['".$id."'] = 'none';\n";
	}
}

function selectMultiple($id, $offset)
{
	$return = "";
	$h = 0;
	if (Input::has($id))
	{
		foreach (Input::get($id) as $rep)
		{
			if ($rep == "sesaAutre")
			{
				if ($h == 0)
				{
					$return .= Input::get($id."_autre");
					$h++;
				}
				else
				{
					if (Input::has($id."_autre"))
					{
						$autre = Input::get($id."_autre");
					}	
					else
					{
						$autre = 'none';
					}
					
					$return .= "::".$autre;
				}
			}
			else
			{
				if ($h == 0)
				{
					$return .= $rep;
					$h++;
				}
				else
				{
					$return .= "::".$rep;
				}
			}
		}
	}
	
	if ($h == 0)
	{
		return "\$formRep['".$id."'] = 'none';\n";
	}
	else
	{
		$return = "\$formRep['".$id."'] = '".$return."';\n";
		
		return $return;
	}
}

function texteLong($id, $offset)
{
	if (Input::has($id))
	{
		return "\$formRep['".$id."'] = '".Input::get($id)."';\n";
	}
	else
	{
		return "\$formRep['".$id."'] = 'none';\n";
	}
}

function texteCourt($id, $offset)
{
	if (Input::has($id))
	{
		return "\$formRep['".$id."'] = '".Input::get($id)."';\n";
	}
	else
	{
		return "\$formRep['".$id."'] = 'none';\n";
	}
}

function tabSelect($id, $offset)
{
	$return = "";
	$h = 0;
	
	for($i=1 ; $i<=$offset ; $i++)
	{
		if ($h == 0)
		{
			$return .= Input::get($id."_".$i);
			$h++;
		}
		else
		{
			$return .= "::".Input::get($id."_".$i);
		}
	}
	
	if ($h == 0)
	{
		return "\$formRep['".$id."'] = 'none';\n";
	}
	else
	{
		return "\$formRep['".$id."'] = '".$return."';\n";
	}
}

function tabTexte($id, $offset)
{
	$return = "";
	$h = 0;
	
	for($i=1 ; $i<=$offset ; $i++)
	{
		if (Input::has($id."_".$i))
		{
			if ($h == 0)
			{
				$return .= Input::get($id."_".$i);
				$h++;
			}
			else
			{
				$return .= "::".Input::get($id."_".$i);
			}
		}
		else
		{
			if ($h == 0)
			{
				$return .= "none";
				$h++;
			}
			else
			{
				$return .= "::none";
			}
		}
		

	}
	
	if ($h == 0)
	{
		return "\$formRep['".$id."'] = 'none';\n";
	}
	else
	{
		return "\$formRep['".$id."'] = '".$return."';\n";
	}
}

/*
 * 
 * Destination néant
 * 
function isNotEmpty($champs)
{
	foreach ($champs as $each)
	{
		$info = explode(':', $each);
		
		if (isset($info[2]))
		{
			for ($i=0 ; $i<=$info[2] ; $i++);
			{
				if (Input::has($info[1]."_".$i) and Input::get($info[1]."_".$i) != "none" and Input::get($info[1]."_".$i) != "")
				{
					echo $info[1]."_".$i." : ".Input::get($info[1]."_".$i);
					return true;
				}
			}
		}
		else
		{
			if (Input::has($info[1]) and Input::get($info[1]) != "none" and Input::get($info[1]) != "")
			{
				echo $info[1]." : ".Input::get($info[1]);
				return true;
			}
		}
	}
	
	return false;
}
*/
