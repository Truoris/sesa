<?php
class questionnaire extends BaseController 
{
	public function home()
	{
		$form = formTemplate::where('affichage', '=', 1)->where('actif', '=', 1)->get();
		$url = Config::get('app.url');
		
		return View::make('home', array('form' => $form, 'url' => $url));
	}
	
	public function fiche($id, $slug)
	{
		$form = formTemplate::where('id', '=', $id)->where('actif', '=', 1)->first();
		
		if ($form)
		{
			eval($form->template);
			
			include('formFonctions.php');
			
			if (Input::has('envoi') and isset($champs) and count($champs) > 0)
			{
				$requete = "";
				
				foreach ($champs as $each)
				{
					$info = explode(':', $each);
					
					if (!isset($info[2]))
					{
						$info[2] = 0;
					}
					
					$requete .= call_user_func($info[0], $info[1], $info[2]);
				}
				
				eval($requete);
				
				$nomClass = "S".$form->id.preg_replace("(-)", '', $form->slug);
				$reponse = new $nomClass;
				
				$reponse->date = time();
				
				foreach ($formRep as $key => $each)
				{
					$reponse->$key = $each;
				}
				
				$reponse->save();
				
				return View::make('fiche-template/'.$id.'-'.$slug, array('mdp' => false, 'id' => $id));
				
			}
			else
			{
				if ($form->mdp == "none")
				{
					$mdp = false;
				}
				else
				{
					$mdp = true;
				}
				return View::make('fiche-template/'.$id.'-'.$slug, array('mdp' => $mdp, 'id' => $id));
			}
		}
		else
		{
			App::abort(404, 'Questionnaire non trouvé');
		}
	}
	
	public function mdpCheck()
	{
		if (Input::has('mdp'))
		{
			$id = Input::get('id');
			$form = formTemplate::where('id', '=', $id)->where('actif', '=', 1)->first();
			
			if (Hash::check(Input::get('mdp'), $form->mdp))
			{
				return 'ok';
			}
			else
			{
				return 'none';
			}
		}
		else
		{
			 return 'none';
		}
	}
}

