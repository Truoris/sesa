<?php
class formTemplate extends Eloquent {
 
	protected $table = 'form_template';
	public $timestamps = false;
	
	public $password;
	public $userName;
	public $nbreRep;
	
	public function getDateAttribute($value)
	{
		return date('d/m/Y H:i', $value);
	}
	
	public function getAffichageAttribute($value)
	{
		if ($value == 0)
		{
			return "Non";
		}
		else
		{
			return "Oui";
		}
	}
	
	public function getActifAttribute($value)
	{
		if ($value == 0)
		{
			return "Non";
		}
		else
		{
			return "Oui";
		}
	}
}
